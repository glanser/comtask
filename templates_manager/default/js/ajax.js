jQuery(document).ready(function() {
	var errorContainerStartError = '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>';
	var errorContainerStartSuccess = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>';
	var errorContainerEnd = '</div>';

	jQuery('#addTask').click(function() {
		jQuery('.loadingImg').show();
		jQuery.post('/manager/tasks/addTask', jQuery('#addTaskForm').serialize(), function(data) {
			jQuery('.help-inline').html('');
			jQuery('.control-group').removeClass('error');
			var arr = JSON.parse(data);
			if (arr.result == true) {
				jQuery('.errorContainer').html(errorContainerStartSuccess + arr.message + ' <strong><a href="/manager/tasks/show/' + arr.id + '">Перейти...</a></strong>' + errorContainerEnd);
				jQuery('input, textarea').val('');
			} else {
				jQuery('.errorContainer').html(errorContainerStartError + arr.message + errorContainerEnd);
				if (arr.name) {
					jQuery('input#name').next('.help-inline').html(arr.name);
					jQuery('input#name').parents('.control-group').addClass('error');
				}
				if(arr.description) {
					jQuery('textarea#description').next('.help-inline').html(arr.description);
					jQuery('textarea#description').parents('.control-group').addClass('error');
				}
			}
			jQuery('.loadingImg').hide();
		});
		return false;
	});

	jQuery('#startTask, #stopTask').click(function() {
		jQuery('.loadingStart').show();
		var rid = jQuery(this).attr('rid');
		if (jQuery(this).attr('id') == 'startTask') {
			var status = 1;
			var show = '#stopTask';
		} else {
			var status = 0;
			var show = '#startTask';
		}
		jQuery.post('/manager/tasks/changeStatus/' + status + '/' + rid, '', function(data) {
			var arr = JSON.parse(data);
			if (arr.result == true) {
				jQuery('.doTask').hide();
				jQuery('#endTask').show();
				jQuery(show).show();
				jQuery('.errorContainer').html(errorContainerStartSuccess + arr.message + errorContainerEnd);
				if (arr.changeDate)
					jQuery('#changeDate').html(arr.changeDate);
				if(arr.spentTime)
					jQuery('#spentTime').html(parseFloat(jQuery('#spentTime').html()) + arr.spentTime);
				updateHistory();
			} else {
				jQuery('.errorContainer').html(errorContainerStartError + arr.message + errorContainerEnd);
			}
			jQuery('.loadingStart').hide();
		});
		return false;
	});
	
	jQuery('#endTask').click(function() {
		jQuery('.loadingStart').show();
		var rid = jQuery('#endTask').attr('rid');
		jQuery.post('/manager/tasks/changeStatus/2/' + rid, '', function(data) {
			var arr = JSON.parse(data);
			if (arr.result == true) {
				jQuery('.doTask').hide();
				jQuery('#startTask').show();
				jQuery('#closeTask').show();
				jQuery('.errorContainer').html(errorContainerStartSuccess + arr.message + errorContainerEnd);
				if (arr.changeDate)
					jQuery('#changeDate').html(arr.changeDate);
				if(arr.spentTime)
					jQuery('#spentTime').html(parseFloat(jQuery('#spentTime').html()) + arr.spentTime);
				updateHistory();
			} else {
				jQuery('.errorContainer').html(errorContainerStartError + arr.message + errorContainerEnd);
			}
			jQuery('.loadingStart').hide();
		});
		return false;
	});

	jQuery('#closeTask').click(function() {
		jQuery('.loadingStart').show();
		var rid = jQuery('#endTask').attr('rid');
		jQuery.post('/manager/tasks/changeStatus/3/' + rid, '', function(data) {
			var arr = JSON.parse(data);
			if(arr.result == true) {
				jQuery('.doTask').hide();
				jQuery('#closedTask').show();
				jQuery('.errorContainer').html(errorContainerStartSuccess + arr.message + errorContainerEnd);
			} else {
				jQuery('.errorContainer').html(errorContainerStartError + arr.message + errorContainerEnd);
			}
			jQuery('.loadingStart').hide();
		});
		return false;
	});

	function updateHistory() {
		jQuery('#taskHistory .historyBody').html('');
		jQuery('#taskHistory').click();
	};

	jQuery('a[href=#taskHistory], #taskHistory').click(function() {
		var rid = jQuery('#startTask').attr('rid');
		if (jQuery('#taskHistory .historyBody').html().trim() == "") {
			jQuery('.loadingHistory').show();
			jQuery.post('/manager/tasks/taskHistory/' + rid, '', function(data) {
				jQuery('#taskHistory .historyBody').html(data);
				jQuery('.loadingHistory').hide();
			});
		}
	});

	jQuery('#closedAjaxLoad').click(function() {
		var path = '.tasklist.closed';
		if (jQuery(path).html().trim() == '') {
			jQuery('.loadingHistory').show();
			jQuery(path).load(location.pathname, { page:0, show:"closed"}, function(data) {
				jQuery(path).html(data);
				jQuery('.loadingHistory').hide();
				jQuery('.pagination a').unbind('click');
				reloadAjax();
			});
		}
	});

	function reloadAjax() {
		jQuery('.pagination a').click(function() {
			var href = jQuery(this).attr('href');
			if (href == '#')
				return false;
			var tab = jQuery(this).parents('div.pagination ul').attr('class');
			jQuery('#' + tab + ' .upperTasklist').addClass('show');
			var path = '.tasklist.' + tab;
			jQuery(path).load(href, { show: tab}, function(data) {
				jQuery('.upperTasklist').removeClass('show');
				jQuery(path).html(data);
				jQuery('.loadingHistory').hide();
				jQuery('.pagination a').unbind('click');
				reloadAjax();
			});
			return false;
		});
	}
	reloadAjax();
});