					<ul class="nav nav-list bs-docs-sidenav affix-top">
<?php foreach($links as $link => $anchor) : ?>
						<li <?php if ('/'.$this->uri->uri_string() == $link) :?> class="active"<?php endif; ?>>
							<a href="<?php echo $link; ?>">
								<i class="icon-chevron-right"></i>
								<?php echo $anchor; ?>
							</a>
						</li>
<?php endforeach; ?>
					</ul>