<?php 
	//Генерируем таблицу
	//Переименовываем названия колонок из базы
	$tableHeadNames = array(
		'id' => '#',
		'query' => 'Запрос',
		'site' => 'Сайт',
		'position' => 'Позиция',
	);
	$tableHead = $this->showhelper->nameReplacer($tableHead, $tableHeadNames);

	$tmpl = array('table_open' => '<table class="table table-striped">');
	$this->table->set_template($tmpl);
	$this->table->set_heading($tableHead);
	echo $this->table->generate($tasks); 
?>