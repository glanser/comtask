<?php if(!$tasks): ?>
<div class="alert alert-error">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  Задач <strong>не найдено</strong>.
</div>
<?php else: ?>
<?php 
	//Генерируем таблицу
	//Переименовываем названия колонок из базы
	$tableHeadNames = array(
		'id' => '#',
		'name' => 'Название',
		'priority' => 'Приоритет',
		'username' => 'Автор',
		'spentTime' => 'Время',
		'price' => 'Стоимость',
		'createDate' => 'Дата создания',
		'changeDate' => 'Дата изменения',
		'status' => 'Статус',
	);
	$tableHead = $this->showhelper->nameReplacer($tableHead, $tableHeadNames);

	$tmpl = array('table_open' => '<table class="table table-striped">');
	$this->table->set_template($tmpl);
	$this->table->set_heading($tableHead);
	echo $this->table->generate($tasks); 
?>
<?php endif; ?>
<div class="pagination">
	<?php echo $pagination; ?>
</div>