<h2 class="pull-left"><?php echo $name; ?></h2><a href="/manager/tasks/add/<?php echo $slug; ?>" class="btn btn-primary pull-right addTask">Добавить задачу</a>
<div class="clearfix"></div>
<div>
  <ul class="projectAbout">
    <li>Автор проекта: <strong><?php echo $username; ?></strong></li>
    <li>Стоимость: <strong><?php echo $price; ?></strong> р/ч</li>
    <li>Задач открыто: <strong><?php echo $count_opened; ?></strong></li>
    <li>Задач закрыто: <strong><?php echo $count_closed; ?></strong></li>
  </ul>
</div>
<ul class="nav nav-tabs" id="tab">
  <li class="active"><a href="#opened">Открытые</a></li>
  <li><a href="#closed" id="closedAjaxLoad">Закрытые</a></li>
</ul>
<div class="tab-content">
  <div class="tab-pane active" id="opened">
  	<div class="listBody">
  		<div class="upperTasklist">
  			<div class="relBlock">
	  			<div class="loading">
  					<img src="/templates_manager/default/img/loading.gif"> Загрузка...
  				</div>
  			</div>
		</div>
		<div class="tasklist opened">
			<?php echo $widget['tasklist']; ?>
		</div>
	</div>
  </div>
  <div class="tab-pane active" id="closed">
  	<img class="loadingHistory" src="/templates_manager/default/img/loading.gif">
  	<div class="listBody">
  		<div class="upperTasklist">
  			<div class="relBlock">
	  			<div class="loading">
  					<img src="/templates_manager/default/img/loading.gif"> Загрузка...
  				</div>
  			</div>
		</div>
  		<div class="tasklist closed">
  		</div>
  	</div>
  </div>
</div>
<script>
$('#tab a').click(function (e) {
  e.preventDefault();
  $(this).tab('show');
});
</script>