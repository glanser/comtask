<h2 class="pull-left">Добавить задачу</h2>
<div class="clearfix"></div>
<div class="errorContainer"></div>
<form class="form-horizontal" method="post" id="addTaskForm">
	<div class="control-group">
		<label class="control-label" for="name">Название</label>
		<div class="controls">
			<input type="text" id="name" placeholder="Название" name="name">
			<span class="help-inline"></span>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="description">Описание</label>
		<div class="controls">
			<textarea id="description" placeholder="Описание" class="span7" name="description"></textarea>
			<span class="help-inline"></span>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="price">Цена за час</label>
		<div class="controls">
			<input type="text" id="price" placeholder="Цена" name="price" value="0">
			<span class="help-inline"></span>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="priority">Приоритет</label>
		<div class="controls">
			<select id="priority" name="priority">
				<?php
					foreach($priority as $value) {
						if (3 == $value['id']) 
							$checked = "selected";
						else
							$checked = '';
						echo '<option value="'.$value['id'].'" '.$checked.'>'.$value['value'].'</option>';
					}
				?>
			</select>
			<span class="help-inline"></span>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="project">Проект</label>
		<div class="controls">
			<select id="project" name="project">
				<?php
					foreach($links as $value) {
						if ($slug == $value['slug']) 
							$checked = "selected";
						else
							$checked = '';
						echo '<option value="'.$value['id'].'" '.$checked.'>'.$value['name'].'</option>';
					}
				?>
			</select>
			<span class="help-inline"></span>
		</div>
	</div>
	<div class="control-group">
		<div class="controls">
			<button type="submit" id="addTask" class="btn btn-primary">Добавить</button><img class="loadingImg" src="/templates_manager/default/img/loading.gif">
		</div>
	</div>
</form>