<h2 class="pull-left"><?php echo $name; ?></h2>
<a href="/manager/tasks/show/<?php echo $id; ?>" rid="<?php echo $id; ?>" id="endTask" class="btn btn-success pull-right endTask doTask" <?php if($status >= 2) echo 'style="display: none;"'; ?>>Решена</a>
<div id="closedTask" class="pull-right doTask red endTask closed" <?php if($status != 3) echo 'style="display: none;"'; ?>><strong>Задача закрыта</strong></div>
<a href="/manager/tasks/show/<?php echo $id; ?>" rid="<?php echo $id; ?>" id="closeTask" class="btn btn-danger pull-right endTask doTask" <?php if($status != 2) echo 'style="display: none;"'; ?>>Закрыта</a>
<a href="/manager/tasks/show/<?php echo $id; ?>" rid="<?php echo $id; ?>" id="startTask" class="btn btn-primary pull-right addTask doTask" <?php if ($status == 1 || $status == 3) echo 'style="display: none;"'; ?>>Начать</a>
<a href="/manager/tasks/show/<?php echo $id; ?>" rid="<?php echo $id; ?>" id="stopTask" class="btn btn-danger pull-right addTask doTask" <?php if ($status != 1) echo 'style="display: none;"'; ?>>&nbsp;Стоп&nbsp;&nbsp;</a>
<img class="addTask pull-right loadingStart" src="/templates_manager/default/img/loading.gif">
<div class="clearfix"></div>
<div class="errorContainer"></div>

<ul class="nav nav-tabs" id="tab">
  <li class="active"><a href="#about">Описание</a></li>
  <li><a href="#taskHistory">История изменений</a></li>
</ul>
 
<div class="tab-content">
  <div class="tab-pane active" id="about">
  	<p>Дата создания: <?php echo $createDate;?></p>
	<p>Дата изменения: <span id="changeDate"><?php echo $changeDate;?></span></p>
	<p>Затраченное время: <span id="spentTime"><?php echo round($spentTime, 2);?></span></p>
	<p>Проект: <?php echo $idProject; ?></p>
	<p>Описание:</p> 
	<p><?php echo nl2br($description); ?></p>
  </div>
  <div class="tab-pane" id="taskHistory">
  	<img class="loadingHistory" src="/templates_manager/default/img/loading.gif">
  	<div class="historyBody">
  	</div>
  </div>
</div>
<script>
$('#tab a').click(function (e) {
  e.preventDefault();
  $(this).tab('show');
});
</script>