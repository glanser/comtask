<h2><?php echo $h1; ?></h2>
<div>
<?php echo form_open('', array('class' => 'form-horizontal')); ?>
	<?php if (isset($error)) : ?>
	<div class="alert alert-block alert-error">
		<?php echo $error; ?>
	</div>
	<?php elseif(isset($success)): ?>
	<div class="alert alert-block alert-success">
		<?php echo $success; ?>
	</div>
	<?php endif; ?>
	<div class="clearfix">
		<?php echo form_submit(array('value' => 'Удалить', 'class' => 'pull-right btn btn-danger', 'style' => 'margin: 0px 10px;')); ?>
		<?php echo form_submit(array('value' => 'Сохранить', 'class' => 'pull-right btn btn-primary')); ?>
	</div>
	<div class="control-group">
		<label class="control-label" for="header">H1</label>
		<div class="controls">
			<?php echo form_input(array('name' => 'header', 'value' => $text['header'], 'class' => 'span6')); ?>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="title">Title</label>
		<div class="controls">
			<?php echo form_input(array('name' => 'title', 'value' => $text['title'], 'class' => 'span6')); ?>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="description">Description</label>
		<div class="controls">
			<?php echo form_textarea(array('name' => 'description', 'value' => $text['description'], 'class' => 'span6', 'rows' => '3')); ?>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="text">Text</label>
		<div class="controls">
			<?php echo form_textarea(array('name' => 'text', 'value' => $text['text'], 'class' => 'span6', 'rows' => '10')); ?>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="text">Alias</label>
		<div class="controls">
			<?php echo form_input(array('name' => 'alias', 'value' => $text['alias'], 'class' => 'span6')); ?>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="text">Key</label>
		<div class="controls">
			<?php echo form_input(array('name' => 'key', 'value' => $text['key'], 'class' => 'span6')); ?>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="text">Date</label>
		<div class="controls">
			<?php echo form_input(array('name' => 'date', 'value' => $text['date'], 'class' => 'span6')); ?>
		</div>
	</div>
<?php echo form_close(); ?>
</div>
