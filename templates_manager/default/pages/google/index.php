<h2><?php echo $h1; ?></h2>
<form method="post">
  <fieldset>
  	<?php if (isset($start)) : ?>
	<legend><?php echo $start; ?></legend>
	<?php else : ?>
    <legend><?php include $_SERVER['DOCUMENT_ROOT'].'/checker.php'; ?></legend>
    <?php endif; ?>
    <table class="startGoogle">
    	<tr>
    		<td>
    			<select name="core">
    				<option value = "0">Выберите проект</option>
    				<?php foreach ($coreList as $value) :?>
    				<option value = "<?php echo $value['id']; ?>"><?php echo $value['site']; ?></option>
    				<?php endforeach; ?>
    			</select>
    		</td>
    		<td>
    			<select name="region">
    				<option value = "0">Выберите регион</option>
    				<?php foreach ($regionList as $value) :?>
    				<option value = "<?php echo $value['id']; ?>"><?php echo $value['region']; ?></option>
    				<?php endforeach; ?>
    			</select>
    		</td>
            <td>
                <select name="deep">
                    <option value = "0">Выберите глубину</option>
                    <option value = "0">1-я страница</option>
                    <option value = "10">2-я страница</option>
                    <option value = "20">3-я страница</option>
                    <option value = "30">4-я страница</option>
                    <option value = "40">5-я страница</option>
                </select>
            </td>
    		<td>
    			<button type="submit" class="btn">Старт</button>
    		</td>
    	</tr>
    </table>
  </fieldset>
</form>

<legend>Мини-FAQ</legend>
<p class="text-error">Капча. Словил ее таки вчера. Увеличил время на delay. Придумал обход капчи. Искренне советую ставить глубину не больше 1 странцы. Использовать глубину больше - если уж очень надо. Вылетит капча - все встанет.</p>
<p>Сборщик в один момент времени может работать только с одной задачей! Скорость работы - 5-6 запросов в минуту. Попробую в дальнейшем сократить это время. Но все будет зависеть от гугла.</p>
<p>Есть вероятность, что появится вторая форма, и будет два сборщика, которые можно будет нагружать двумя задачами одновременно.</p>
<p>Процесс сбора пока что никак не отслеживается. Но всегда можно зайти в проект и просмотреть результаты сбора. Объем результатов увеличивается по мере обработки.</p>