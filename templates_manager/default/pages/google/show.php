<h2 class="pull-left"><?php echo $site; ?></h2>
<a href="/manager/google/core/<?php echo $id; ?>" class="btn btn-primary pull-right" style="margin-top: 15px;">Изменить ядро</a>
<?php if ($idRegion != 0): ?>
<div class="pull-right" style="margin-top: 15px;">
	<form method="post">
		<table class="startGoogle">
			<tr>
				<td>
						<select name="date">
							<?php foreach($dates as $value) : ?>
							<option value="<?php echo $value['id']; ?>"><?php echo date('Y-m-d H:i:s', $value['dateStart']); ?></option>
							<?php endforeach; ?>
						</select>
				</td>
				<td>
					<button type="submit" class="btn btn-primary">Показать</button>
				</td>
			<tr>
		</table>
	</form>
</div>
<?php endif; ?>
<div class="clearfix"></div>
<?php if ($idRegion == 0): ?>
	<legend>Выберите регион:</legend>
	<?php foreach($regions as $value) : ?>
		<p><a href="/manager/google/show/<?php echo $id.'/'.$value['idRegion']; ?>"><?php echo $value['region']; ?></a></p>
	<?php endforeach; ?>
<?php elseif(isset($idStart) || $idRegion != 0) : ?>
	<?php echo $results['corelist']; ?>
<?php endif; ?>