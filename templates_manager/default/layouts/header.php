<!DOCTYPE HTML>
<html>
<head>
	<title><?php if(isset($title)) echo $title.' - '; ?>Менеджер задач "comTask"</title>
<?php if (isset($description)) : ?>
	<meta name="description" content="<?php echo $description; ?>">
<?php endif; ?>
<?php if (isset($keywords)) : ?>
	<meta name="keywords" content="<?php echo $keywords; ?>">
<?php endif; ?>
	<meta charset="utf-8">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
	<script src="<?php echo $path ?>/js/bootstrap.min.js"></script>
	<script src="<?php echo $path ?>/js/ajax.js"></script>
<?php
	require "lessc.inc.php";
	$less = new lessc;
	$less->compileFile($_SERVER['DOCUMENT_ROOT'].$path."/css/"."style.less", $_SERVER['DOCUMENT_ROOT'].$path."/css/"."style.css");
?>
	<link rel="stylesheet" href="<?php echo $path; ?>/css/style.css">
	<link rel="stylesheet" href="<?php echo $path; ?>/css/bootstrap.min.css">
</head>
<body>
	<div id="wrap">
		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="navbar-inner">
				<div class="logout-brand pull-right">
					<a class="logout brand" href="/manager/auth/logout">Выход</a>
					<a class="brand" href="/manager/">comTask</a>
				</div>
				<div class="nav-collapse collapse">
<?php echo $widget['menu']; ?>
				</div>
			</div>
		</div>
		<div class="container-fluid main">
			<div class="row-fluid">
				<div class="span3 span3 bs-docs-sidebar">
<?php echo $widget['leftmenu']; ?>
				</div>
				<div class="span9 main-container">

