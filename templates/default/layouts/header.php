<!DOCTYPE HTML>
<html>
<head>
	<title><?php if(isset($title)) echo $title; ?></title>
<?php if (isset($description)) : ?>
	<meta name="description" content="<?php echo $description; ?>">
<?php endif; ?>
<?php if (isset($keywords)) : ?>
	<meta name="keywords" content="<?php echo $keywords; ?>">
<?php endif; ?>
	<meta charset="utf-8">
</head>
<body>
