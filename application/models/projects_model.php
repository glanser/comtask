<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projects_model extends CI_Model {

	protected $dbname = 'projects';
	protected $dbnameUsers = 'users';

	public function __construct() {
		parent::__construct();
	}

	public function get_list() {
		$this->db->select('name, slug, id'); 
		$query = $this->db->get($this->dbname);
		$result = $query->result_array();
		return $result;
	}

	public function get_by_slug($slug) {
		$this->db->select($this->dbname.'.id, slug, price, username, name, idUser');
		$this->db->from($this->dbname);
		$this->db->where(array('slug' => $slug));
		$this->db->join($this->dbnameUsers, $this->dbnameUsers.'.id = '.$this->dbname.'.idUser');
		$query = $this->db->get();
		$result = $query->result_array();
		if (isset($result[0]))
			return $result[0];
		else
			return false;
	}
}