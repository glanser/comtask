<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Google_model extends CI_Model {

	protected $dbCore = 'core';
	protected $dbQuery = 'query';
	protected $dbRegion = 'region';
	protected $dbResult = 'result';
	protected $dbStart = 'start';

	public function __construct() {
		parent::__construct();
	}

	public function get_list() {
		$this->db->select('id, site');
		$query = $this->db->get($this->dbCore);
		$result = $query->result_array();
		return $result;
	}

	function get_core_by_id($id = 0) {
		if ($id == 0)
			return false;

		$this->db->select('site, id');
		$this->db->from($this->dbCore);
		$this->db->where(array('id' => $id));
		$query = $this->db->get();
		$result = $query->result_array();
		if (isset($result[0]))
			return $result[0];
		else
			return false;
	}

	function get_regions() {
		$this->db->select('id, region');
		$query = $this->db->get($this->dbRegion);
		$result = $query->result_array();
		return $result;
	}

	function get_query_by_core($id = 0) {
		if ($id == 0)
			return false;

		$this->db->select('id, query');
		$this->db->where(array('idCore' => $id));
		$query = $this->db->get($this->dbQuery);
		$result = $query->result_array();
		return $result;
	}

	function get_start($id = 0) {
		if ($id == 0)
			return false;

		$this->db->select('id, query');
		$this->db->where(array('idCore' => $id));
		$query = $this->db->get($this->dbQuery);
		$result = $query->result_array();
		return $result;
	}

	function get_regions_by_result($idCore) {
		$this->db->select('region, idRegion');
		$this->db->join($this->dbStart, $this->dbStart.'.idRegion = '.$this->dbRegion.'.id');
		$this->db->where(array($this->dbStart.'.idCore' => $idCore));
		$this->db->distinct();
		$query = $this->db->get($this->dbRegion);
		$result = $query->result_array();
		return $result;
	}

	function get_starts_by_region($idCore = 0, $idRegion = 0) {
		if ($idCore == 0 || $idRegion == 0)
			return false;

		$this->db->select('id, dateStart');
		$this->db->where(array('idCore' => $idCore, 'idRegion' => $idRegion));
		$this->db->order_by('dateStart', 'desc');
		$query = $this->db->get($this->dbStart);
		$result = $query->result_array();
		return $result;
	}

	function get_result_by_idstart($idStart) {
		if ($idStart == 0)
			return false;

		$this->db->select($this->dbCore.'.`site`, '.$this->dbQuery.'.`query`, '.$this->dbResult.'.`position`, '.$this->dbResult.'.`link`');
		$this->db->join($this->dbCore, $this->dbCore.'.id = '.$this->dbStart.'.idCore');
		$this->db->join($this->dbQuery, $this->dbQuery.'.idCore = '.$this->dbCore.'.id');
		$this->db->join($this->dbResult, $this->dbResult.'.idQuery = '.$this->dbQuery.'.id AND '.$this->dbResult.'.idStart = '.$this->dbStart.'.id');
		$this->db->where(array($this->dbStart.'.id' => $idStart));
		$query = $this->db->get($this->dbStart);
		$result = $query->result_array();
		return $result;
	}

	function get_result_last($idCore, $idRegion) {
		if ($idCore == 0 || $idRegion == 0)
			return false;
		$this->db->select('id');
		$this->db->where(array('idCore' => $idCore, 'idRegion' => $idRegion));
		$this->db->order_by('dateStart', 'desc');
		$query = $this->db->get($this->dbStart);
		$result = $query->result_array();
		return $this->get_result_by_idstart($result[0]['id']);
	}

	/*public function get_by_slug($slug) {
		$this->db->select($this->dbname.'.id, slug, price, username, name, idUser');
		$this->db->from($this->dbname);
		$this->db->where(array('slug' => $slug));
		$this->db->join($this->dbnameUsers, $this->dbnameUsers.'.id = '.$this->dbname.'.idUser');
		$query = $this->db->get();
		$result = $query->result_array();
		if (isset($result[0]))
			return $result[0];
		else
			return false;
	}*/
}