<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Text_model extends CI_Model {

	protected $dbname = 'pages';

	public function __construct() {
		parent::__construct();
	}

	public function get_where($id = 0) {
		$query = $this->db->get_where($this->dbname, array('id_page' => $id));
		$result = $query->result_array();
		return $result[0];
	}

	public function get_interval($from, $offset) {
		$query = $this->db->get($this->dbname, $offset, $from);
		$result = $query->result_array();
		return $result;
	}
}