<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tasks_model extends CI_Model {

	protected $dbname = 'tasks';
	protected $dbnameChanges = 'tasks_changes';
	protected $dbnamePriorities = 'priorities';
	protected $dbnameUser = 'users';

	function __construct() {
		parent::__construct();
	}

	function get_where_project($id = 0, $show = NULL, $start, $offset) {
		if ($id == 0) 
			return false;

		$where = array('idProject' => $id);
		if ($show != NULL)
			$where[$show[0]] = $show[1];

		$select = $this->dbname.'.id, idProject, '.$this->dbname.'.name, '.$this->dbnamePriorities.'.value as priority, username, spentTime, price, createDate, changeDate, status';

		$this->db->select($select);
		$this->db->from($this->dbname);
		$this->db->join($this->dbnamePriorities, $this->dbname.'.priority = '.$this->dbnamePriorities.'.id');
		$this->db->join($this->dbnameUser, $this->dbname.'.idUser = '.$this->dbnameUser.'.id');
		$this->db->where($where);
		$this->db->order_by("id", "desc");
		$this->db->limit($offset, $start);
		$query = $this->db->get();

		//$query = $this->db->get_where($this->dbname, $where, $offset, $start);
		$result = $query->result_array();
		
		if (empty($result))
			return false;
		
		return $result;
	}

	function get_by_id($id) {
		$query = $this->db->get_where($this->dbname, array('id' => $id));
		$result = $query->result_array();
		if (isset($result[0]))
			return $result[0];
		else false;
	}

	function update_where_id($id, $data) {
		if (empty($data))
			return false;

		$this->db->where('id', $id);
		if($this->db->update($this->dbname, $data))
			return true;
		else
			return false;
	}

	function count_where($arrWhere) {
		$this->db->select("COUNT(*) AS count");
		$query = $this->db->get_where($this->dbname, $arrWhere);
		$result = $query->result_array();
		return $result[0]['count'];
	}

	function addNewTask($arrTask) {
		$date = date('Y-m-d H:i:s');
		$data = array(
			'idProject' => $arrTask['project'], 
			'name' => $arrTask['name'], 
			'description' => $arrTask['description'],
			'priority' => $arrTask['priority'],
			'price' => $arrTask['price'],
			'createDate' => $date,
			'changeDate' => $date,
			'idUser' => $this->session->userdata['user_id'],
		);
		if($this->db->insert($this->dbname, $data))
			return $this->db->insert_id();
		else
			return false;
	}

	function addTask($id, $date) {
		$data = array('idTask' => $id, 'startDate' => $date, 'idUser' => $this->session->userdata['user_id'],);
		if($this->db->insert($this->dbnameChanges, $data))
			return true;
		else
			return false;
	}

	function stopTask($id, $date) {
		//get the startDate
		$query = $this->db->get_where($this->dbnameChanges, array('idTask' => $id, 'endDate' => '0000-00-00 00:00:00'));
		$time = $query->result_array();

		//update endDate
		$this->db->where('idTask', $id);
		$this->db->where('endDate', '0000-00-00 00:00:00');
		$data = array('endDate' => $date);
		if($this->db->update($this->dbnameChanges, $data))
			return (strtotime($date) - strtotime($time[0]['startDate']))/3600;
		else
			return false;
	}

	function update_spenttime($id, $time) {
		if($this->db->query("UPDATE `".$this->dbname."` SET `spentTime` = `spentTime` + '".$time."' WHERE id = ".$id.""))
			return true;
		else 
			return false;

	}

	function countNotEnded($id) {
		$this->db->where('idTask', $id);
		$this->db->where('endDate', '0000-00-00 00:00:00');
		$this->db->from($this->dbnameChanges);
		return $this->db->count_all_results();
	}

	function getChanges($id) {
		$query = $this->db->query("SELECT `".$this->dbnameChanges."`.`id`, `username`, `startDate`, `endDate` FROM `".$this->dbnameChanges."`, `users` WHERE idTask = ".$id." AND `users`.`id` = `idUser` ORDER by id DESC;");
		return $result = $query->result_array();
	}

	function getListPriorities() {
		$query = $this->db->get($this->dbnamePriorities);
		return $query->result_array();
	}
}