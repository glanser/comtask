<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class menu_widget extends widget {

	function __construct() {
		parent::__construct();
	}

	public function render($options) {
		$widget = $this->CI->load->view($options['view'], $options['data'], true);
		return $widget;
	}
}