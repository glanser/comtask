<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tasklist_widget extends widget {

	function __construct() {
		parent::__construct();
	}

	public function render($options) {

		$per_page = $this->CI->chconf->params['tasks_per_page'];
		$result = $options['data'];

		$showing = array(
			'opened' => array('status !=', 3),
			'closed' => array('status', 3),
			'all' => NULL,
		);

		$result['tasks'] = $this->CI->Tasks_model->get_where_project($result['id'], $showing[$options['show']], $options['page'], $per_page);

		$idProject = $result['tasks'][0]['idProject'];

		if ($result['tasks'] != false) {
			// Optimize Data
			$result['tasks'] = $this->CI->showhelper->generateLinks($result['tasks'], $result['price']);
			
			/**
			 * @todo delete this shit and add array for "select from db"
			 */
			$result['tasks'] = $this->CI->showhelper->optimize($result['tasks'], array('idProject', 'description'));
			//Generate table heading
			foreach($result['tasks'][0] as $key => $name) {
				$result['tableHead'][] = $key;
			}
		}

		$config['base_url'] = '/manager/projects/show/'.$options['data']['slug'];

		$config['uri_segment'] = 5;
		$config['total_rows'] = $this->CI->Tasks_model->count_where(array($showing[$options['show']][0] => $showing[$options['show']][1], 'idProject' => $idProject));
		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<ul class="'.$options['show'].'">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo;';
		$config['next_link'] = '&raquo;';
		$config['first_link'] = 'В начало';
		$config['last_link'] = 'В конец';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';


		$this->CI->pagination->initialize($config); 

		$result['pagination'] = $this->CI->pagination->create_links();

		$widget = $this->CI->load->view($options['view'], $result, true);
		return $widget;
	}
}