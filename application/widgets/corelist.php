<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class corelist_widget extends widget {

	function __construct() {
		parent::__construct();
	}

	public function render($options) {
		$result = $options['data'];

		/**
		 * @todo delete this shit and add array for "select from db"
		 */
		$result['tasks'] = $this->CI->showhelper->optimize($result, array());
		//Generate table heading
		foreach($result['tasks'][0] as $key => $name) {
			$result['tableHead'][] = $key;
		}

		$widget = $this->CI->load->view($options['view'], $result, true);
		return $widget;
	}
}