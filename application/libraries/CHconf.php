<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
* Name:  CHconf
*
* Author: Sergey Ananskikh
*		  glansers@gmail.com
*
* Created:  31.03.2013
*
* Description:  Load custom configs like language or template from database.
*
*/
class CHconf {

	public $params;
	protected $CI;

	public function __construct() {
		$this->CI =& get_instance();
		$this->CI->load->database();
		$query = $this->CI->db->get('config');
		$result = $query->result_array();
		foreach($result as $each) {
			$this->params[$each['name']] = $each['param'];
		}
	}
}