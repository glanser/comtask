<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class showhelper {

	function nameReplacer($tableHead, $tableHeadNames) {
		foreach ($tableHead as $key => $value) {
			if (isset($tableHeadNames[$value]))
				$tableHead[$key] = $tableHeadNames[$value];
		}
		return $tableHead;
	}

	function optimize($array, $columns)	{
		foreach($array as $key => $value) {
			foreach($columns as $column)
			unset($array[$key][$column]);
		}
		return $array;
	}

	function generateLinks($array, $price = 0) {
		foreach ($array as $key => $value) {
			$array[$key]['spentTime'] = round($array[$key]['spentTime'], 2);

			if($array[$key]['price'] != 0)
				$array[$key]['price'] = $array[$key]['spentTime'] * $array[$key]['price'];
			else
				$array[$key]['price'] = $array[$key]['spentTime'] * $price;

			$array[$key]['name'] = '<a href="/manager/tasks/show/'.$array[$key]['id'].'">'.$array[$key]['name'].'</a>';
			
			if ($array[$key]['status'] == 0 && $array[$key]['changeDate'] == $array[$key]['createDate'])
				$array[$key]['status'] = '<span class="blue">Новая</span>';
			elseif ($array[$key]['status'] == 0)
				$array[$key]['status'] = '<span class="yellow">Приостановлена</span>';
			elseif ($array[$key]['status'] == 1)
				$array[$key]['status'] = '<span class="green">В работе</span>';
			elseif ($array[$key]['status'] == 2)
				$array[$key]['status'] = '<span class="red">Решена</span>';
			elseif ($array[$key]['status'] == 3)
				$array[$key]['status'] = '<strong>Закрыта</strong>';
		}
		return $array;
	}
}