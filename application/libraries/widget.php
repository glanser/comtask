<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class widget {

	public function __construct() {
		$this->CI =& get_instance();
	}

	/**
	 * Method get the widget, make object and render it.
	 * @var option is array
	 * @return string
	 */

	public function get($widget, $options) {
		$widgetPath = $_SERVER['DOCUMENT_ROOT'].'/'.APPPATH.'widgets/'.$widget.'.php';
		if (!file_exists($widgetPath)) {
			return false;
		}
		require_once($widgetPath);
		$classWidget = $widget.'_widget';
		$widgetObj = new $classWidget;
		return $widgetObj->render($options);
	}
}