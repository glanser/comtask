<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CI_DBroutes {

	private $dbConfig;

	function __construct() {
		include $_SERVER['DOCUMENT_ROOT'].'/application/config/database.php';
		$this->dbConfig = $db['default'];
	}

	public function get() {
		if ($this->dbConfig['dbdriver'] != 'mysql')
			return;

		$link = mysql_connect($this->dbConfig['hostname'], $this->dbConfig['username'], $this->dbConfig['password']);
		$db_selected = mysql_select_db($this->dbConfig['database'], $link);
		$result = mysql_query('SELECT * FROM '.$this->dbConfig['dbprefix'].'routes');
		while($res = mysql_fetch_array($result)) {
			$route[$res['url']] = $res['route'];
		}
		return $route;
    }
}