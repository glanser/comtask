<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CMS_Controller extends CI_Controller {

	public $authMethod;
	public $templatePath;
	public $basepath;

	function __construct($auth = 'web') {
		parent::__construct();

		$this->authMethod = $auth;

		if ($auth == "manager") {

			// TODO: autorization is here
			$this->load->library('ion_auth');
			$this->load->library('form_validation');
			$this->load->helper('url');

			$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

			$this->lang->load('auth');
			$this->load->helper('language');

			if (!$this->ion_auth->logged_in()) {
				if (uri_string() != 'manager/auth/login')
					redirect('manager/auth/login', 'refresh');
			} elseif (!$this->ion_auth->is_admin()) {
				redirect('/manager', 'refresh');
			}

			$template = $this->chconf->params['template_admin'];
			$this->templatePath = '../../templates_manager/'.$template;
			$this->basepath = '/templates_manager/'.$template;
		} elseif ($this->authMethod == 'web') {
			$template = $this->chconf->params['template'];
			$this->templatePath = '../../templates/'.$template;
			$this->basepath = '/templates/'.$template;
		}
	}

	public function render($views = array(), $header_data = array(), $footer_data = array(), $partial_render = "") {

		if($this->authMethod == 'manager') {

			/**
			 * @todo 
			 * load widgets from database and render if it need
			 *
			 */

			$arrayMenu['links'] = array(
				'/manager' => '<i class="icon-home"></i>',
				'/manager/projects' => 'Проекты',
				'/manager/wiki' => 'Вики',
				'/manager/google' => 'Google сборщик',
			);
			$header_data['widget']['menu'] = $this->widget->get('menu', array('data' => $arrayMenu, 'view' => $this->templatePath.'/widgets/menu'));

		}

		$header_data['path'] = $this->basepath;

		$this->load->view($this->templatePath.'/layouts/header.php', $header_data);
		
		foreach ($views as $view => $data) 
			$this->load->view($this->templatePath.'/'.$view.'.php', $data);
		
		$this->load->view($this->templatePath.'/layouts/footer.php', $footer_data);

	}

	public function isAjaxRequest() {
		return (
			isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
			!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
			strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
		) ? true : false;
	}

}