<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>ComTask - simple amd comfortable task manager for freelance.</title>
	<link rel="stylesheet" href="/templates_manager/default/css/bootstrap.min.css">
	<style type="text/css">
	</style>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
	  <div class="navbar-inner">
	  	<div class="container">
		    <a class="brand" href="#">Comtask</a>
		    <ul class="nav">
		      <li class="active"><a href="/">Home</a></li>
		      <li><a href="/manager/">Login</a></li>
		    </ul>
		</div>
	  </div>
	</div>
	<div class="container">
		<div class="row">
			<div class="span12" style="margin-top: 60px;">
				<h1 style="padding-bottom: 0px; margin-bottom: 0px;">ComTask</h1>
				<h2 style="padding-left: 200px; padding-top: 0px; margin-top: 0px;" class="text-info">- easy Task Manager</h2>
			</div>
		</div>
	</div>
</body>
</html>