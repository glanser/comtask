<?php
  $template = $this->chconf->params['template_admin'];
  $this->templatePath = '/templates_manager/'.$template;
?>
<!DOCTYPE HTML>
<html>
<head>
  <title>Авторизация - Менеджер задач "comTask"</title>
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta charset="utf-8">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
  <script src="<?php echo $this->templatePath ?>/js/bootstrap.min.js"></script>
  <script src="<?php echo $this->templatePath ?>/js/ajax.js"></script>
<?php
  require "lessc.inc.php";
  $less = new lessc;
  $less->compileFile($_SERVER['DOCUMENT_ROOT'].$this->templatePath."/css/"."style.less", $_SERVER['DOCUMENT_ROOT'].$this->templatePath."/css/"."style.css");
?>
  <link rel="stylesheet" href="<?php echo $this->templatePath; ?>/css/style.css">
  <link rel="stylesheet" href="<?php echo $this->templatePath; ?>/css/bootstrap.min.css">
</head>
<body>
  <div class="loginWindow bs-docs-sidenav">
    <h2>comTask</h2>
    <div id="infoMessage"><?php echo $message;?></div>
    <?php echo form_open("manager/auth/login");?>

      <p>
        <?php echo lang('login_identity_label', 'indentity');?>
        <?php echo form_input($identity);?>
      </p>

      <p>
        <?php echo lang('login_password_label', 'password');?>
        <?php echo form_input($password);?>
      </p>

      <p>
        <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?> Запомнить меня
      </p>


      <p><?php echo form_submit('submit', 'Вход', 'class="btn btn-primary"');?></p>

    <?php echo form_close();?>

    <p><a href="forgot_password">Забыл пароль?</a></p>
  </div>
</body>
</html>