<?php
	if (!empty($changes)) {
		function nullChanger($data) {
			if ($data == "0000-00-00 00:00:00") return '<span class="green">В работе</span>';
			return $data;
		}
		// Генерируем таблицу
		//Переименовываем названия колонок из базы
		$tableHeadNames = array(
			'id' => '#',
			'username' => 'Пользователь',
			'startDate' => 'Начата',
			'endDate' => 'Остановлена',
			'changeDate' => 'Дата изменения',
			'status' => 'Статус',
		);
		$tableHead = $this->showhelper->nameReplacer($changesTableHead, $tableHeadNames);

		$tmpl = array('table_open' => '<table class="table table-striped">');
		$this->table->set_template($tmpl);
		$this->table->set_heading($tableHead);
		$this->table->function = 'nullChanger';
		echo $this->table->generate($changes);
	} else {
		echo "Истории изменений нет...";
	}
?>