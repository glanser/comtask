<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Google extends CMS_Controller {

	public $leftMenu;
	public $coreList;

	function __construct() {
		parent::__construct('manager');
		$this->load->model('Google_model', '', TRUE);
		$this->coreList = $arrayLinks = $this->Google_model->get_list();
		foreach($arrayLinks as $value)
			$arrayMenu['links']['/manager/google/show/'.$value['id']] = $value['site'];

		$this->leftMenu['widget']['leftmenu'] = $this->widget->get('menu', array('data' => $arrayMenu, 'view' => $this->templatePath.'/widgets/leftmenu'));
	}

	function index($id = '') {
		$header = array('title' => 'Google сборщик');
		$result = array('h1' => 'Google сборщик', 'text' => '');
		if (isset($_POST['core'])) {
			$result['start'] = $this->_startGoogle($_POST['core'], $_POST['region'], $_POST['deep']);
		}
		$result['coreList'] = $this->coreList;
		$result['regionList'] = $this->Google_model->get_regions();
		$this->render(array('pages/google/index' => $result), $header + $this->leftMenu);
	}

	function show($id = 0, $idRegion = 0) {
		if ($id == 0)
			show_404();

		$result = $this->Google_model->get_core_by_id($id);
		$result['regions'] = $this->Google_model->get_regions_by_result($id);
		$result['idRegion'] = $idRegion;
		if ($idRegion != 0) {
			$result['dates'] = $this->Google_model->get_starts_by_region($id, $idRegion);
		}
		if (isset($_POST['date']) && $idRegion != 0) {
			$result['idStart'] = $_POST['date'];
			$result['results'] = $this->_getListResult($result['idStart']);
		} elseif($idRegion != 0) {
			$result['results'] = $this->_getListResult($id, $idRegion);
		}

		$header = array('title' => $result['site']);
		// Render page
		$this->render(array('pages/google/show' => $result), $header + $this->leftMenu);
	}

	function core($id = 0) {
		if ($id == 0)
			show_404();
		$result = $this->core = $this->Google_model->get_core_by_id($id);

		$result['widget'] = $this->_getListCore($id);
		$header = array('title' => $result['site']);
		// Render page
		$this->render(array('pages/google/core' => $result), $header + $this->leftMenu);
	}

	function _getListResult($id, $idRegion = 0) {
		// Load library for widget
		$this->load->library('table');
		$this->load->library('showhelper');

		// Get data
		if ($idRegion == 0)
			$result = $this->Google_model->get_result_by_idstart($id);
		else
			$result = $this->Google_model->get_result_last($id, $idRegion);

		if (!$result)
			show_404();

		//Send to the widget
		$corelistData = array(
			'view' => $this->templatePath.'/widgets/corelist',
			'data' => $result,
		);
		$result['corelist'] = $this->widget->get('corelist', $corelistData);
		return $result;
	}

	function _getListCore($id) {
		// Load library for widget
		$this->load->library('table');
		$this->load->library('showhelper');

		// Get data
		if(!$result = $this->Google_model->get_query_by_core($id))
			show_404();

		//Send to the widget
		$corelistData = array(
			'view' => $this->templatePath.'/widgets/corelist',
			'data' => $result,
		);
		$result['corelist'] = $this->widget->get('corelist', $corelistData);
		return $result;
	}

	function _startGoogle($idCore = 0, $idRegion = 0, $startNumber = 0) {
		if ($idCore == 0 || $idRegion == 0)
			return 'Выберите проект и регион';
		$shell = '#!/bin/bash
php for.php '.$idCore.' '.$idRegion.' '.$startNumber;
		if (file_put_contents('google.sh', $shell)) {
			include 'starter.php';
		} else {
			return $result['start'];
		}
	}

}

