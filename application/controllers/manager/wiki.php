<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wiki extends CMS_Controller {

	public $leftMenu;

	function __construct() {
		parent::__construct('manager');
		$this->load->model('Projects_model', '', TRUE);
		$arrayLinks = $this->Projects_model->get_list();
		foreach($arrayLinks as $value)
			$arrayMenu['links']['/manager/wiki/show/'.$value['slug']] = $value['name'];

		$this->leftMenu['widget']['leftmenu'] = $this->widget->get('menu', array('data' => $arrayMenu, 'view' => $this->templatePath.'/widgets/leftmenu'));
	}

	function index() {
		$header = array('title' => 'Pages');
		$result = array('h1' => 'Pages', 'text' => '');
		$this->render(array('pages/pages/index' => $result), $header + $this->leftMenu);
	}

	function show($slug = false, $page = 0) {
		if ($slug == false)
			show_404();

		$header = array('title' => $result['name']);
		// Render page
		$this->render(array('pages/wiki/show' => $result), $header + $this->leftMenu);
	}

}

