<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tasks extends CMS_Controller {

	public $leftMenu;

	function __construct() {
		parent::__construct('manager');
		$this->load->model('Projects_model', '', true);
		$this->load->model('Tasks_model', '', true);
		$arrayLinks = $this->Projects_model->get_list();
		
		foreach($arrayLinks as $value)
			$arrayMenu['links']['/manager/projects/show/'.$value['slug']] = $value['name'];

		$this->leftMenu['widget']['leftmenu'] = $this->widget->get('menu', array('data' => $arrayMenu, 'view' => $this->templatePath.'/widgets/leftmenu'));
	}

	function index() {
		show_404();
	}

	function show($id = false) {
		if ($id == false)
			show_404();

		$result = $this->Tasks_model->get_by_id($id);

		if (!$result) 
			show_404();

		$header = array('title' => 'Task');

		$this->render(array('pages/tasks/show' => $result), $header + $this->leftMenu);
	}

	function add($slug = false, $notFound = NULL) {
		if ($notFound != NULL)
			show_404();

		$header = array('title' => 'Add task');
		$result['links'] = $this->Projects_model->get_list();
		$result['priority'] = $this->Tasks_model->getListPriorities();
		$result['slug'] = $slug;
		$this->render(array('pages/tasks/add' => $result), $header + $this->leftMenu);
	}

	function addTask() {
		if(!$this->isAjaxRequest())
			show_404();

		$this->load->library('form_validation');
		
		$rules = array(
			array(
				'field' => 'name',
				'rules' => 'required|xss_clean|trim',
			),
			array(
				'field' => 'description',
				'rules' => 'required|xss_clean|trim',
			),
			array(
				'field' => 'project',
				'rules' => 'required|integer',
			),
			array(
				'field' => 'priority',
				'rules' => 'required|integer',
			),
			array(
				'field' => 'price',
				'rules' => 'required|integer',
			),
		);

		$this->form_validation->set_rules($rules);
		
		if ($this->form_validation->run() == false) {
			$error = NULL;
			if(form_error('name')) $error .= ', "name":"'.form_error('name').'"';
			if(form_error('description')) $error .= ', "description":"'.form_error('description').'"';
			if(form_error('project')) $error .= ', "project":"'.form_error('project').'"';
			if(form_error('priority')) $error .= ', "priority":"'.form_error('priority').'"';
			$result['json'] = '{"result":false, "message":"Every fields are <strong>required</strong>."'.$error.'}';
		} else {
			$idTask = $this->Tasks_model->addNewTask($_POST);
			if($idTask)
				$result['json'] = '{"result":true, "message":"Task added <strong>successfully</strong>!", "id":'.$idTask.'}';
		}
		$this->load->view('default/ajax', $result);
	}

	function changeStatus($status = NULL, $id = false) {
		
		if($id == false || $status == NULL || !$this->isAjaxRequest())
			show_404();

		$date = date('Y-m-d H:i:s');

		if ($this->Tasks_model->update_where_id($id, array('status' => $status, 'changeDate' => $date))) {

			if ($status == 1) {
				if (!$this->Tasks_model->countNotEnded($id)) {
					$this->Tasks_model->addTask($id, $date);
					$result['json'] = '{"result":true, "message":"Task <strong>added</strong> to work.", "changeDate":"'.$date.'"}';
				} else {
					$result['json'] = '{"result":false, "message":"Task started somebody. <strong>Please reload page.</strong>", "changeDate":"'.$date.'"}';
				}
			} elseif ($status == 0) {
				if (!$this->Tasks_model->countNotEnded($id)) {
					$result['json'] = '{"result":false, "message":"Task stopped somebody. <strong>Please reload page.</strong>", "changeDate":"'.$date.'"}';
				} else {
					$diffTime = $this->Tasks_model->stopTask($id, $date);
					$this->Tasks_model->update_spenttime($id, $diffTime);
					$result['json'] = '{"result":true, "message":"Task <strong>stopped</strong>.", "changeDate":"'.$date.'", "spentTime":'.round($diffTime, 2).'}';
				}
			} elseif ($status == 2) {
				if ($this->Tasks_model->countNotEnded($id)) {
					$diffTime = $this->Tasks_model->stopTask($id, $date);
					$this->Tasks_model->update_spenttime($id, $diffTime);
				}
				$result['json'] = '{"result":true, "message":"Task <strong>solved</strong>.", "changeDate":"'.$date.'"}';
			} elseif ($status == 3) {
				$result['json'] = '{"result":true, "message":"Task <strong>closed</strong>. You can\'t work with task more.", "changeDate":"'.$date.'"}';
			} else {
				$result['json'] = '{"result":false, "message":"Something happend. Please say about it administrator."}';
			}
		} else {
			$result['json'] = '{"result":false, "message":"Something happend. Please say about it administrator."}';
		}

		$this->load->view('default/ajax', $result);
	}

	function taskHistory($id = false) {
		if($id == false || !$this->isAjaxRequest())
			show_404();

		// Load library
		$this->load->library('table');
		$this->load->library('showhelper');

		$result['changes'] = $this->Tasks_model->getChanges($id);
		if (isset($result['changes'][0])) {
			foreach($result['changes'][0] as $key => $name) {
				$result['changesTableHead'][] = $key;
			}
		}

		$this->load->view('default/taskHistory', $result);
	}

}

