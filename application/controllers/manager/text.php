<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CMS_Controller {

	function __construct() {
		parent::__construct('manager');
		$this->load->model('Text_model', '', TRUE);
	}

	public function index() {
		$header = array('title' => 'Welcome');
		$result = array('h1' => 'Welcome');
		$this->render(array('pages/default/index' => $result), $header);
	}

	public function page($id = 1) {
		//TODO: Add a param and set value per page.
		$param['perpage'] = 20;
		if ($id == 1) {
			$startEntrie = 0;
		} else {
			$startEntrie = ($id - 1) * $param['perpage'];
		}
		print_r($this->Text_model->get_interval($startEntrie, $param['perpage']));
	}

}

