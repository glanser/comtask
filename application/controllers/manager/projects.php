<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projects extends CMS_Controller {

	public $leftMenu;

	function __construct() {
		parent::__construct('manager');
		$this->load->model('Projects_model', '', true);
		$this->load->model('Tasks_model', '', true);
		$arrayLinks = $this->Projects_model->get_list();
		
		foreach($arrayLinks as $value)
			$arrayMenu['links']['/manager/projects/show/'.$value['slug']] = $value['name'];

		$this->leftMenu['widget']['leftmenu'] = $this->widget->get('menu', array('data' => $arrayMenu, 'view' => $this->templatePath.'/widgets/leftmenu'));
	}

	function index() {
		$header = array('title' => 'Pages');
		$result = array('h1' => 'Pages', 'text' => '');
		$this->render(array('pages/pages/index' => $result), $header + $this->leftMenu);
	}

	function show($slug = false, $page = 0) {
		if ($slug == false)
			show_404();

		// Ajax or not ajax???
		if (!$this->isAjaxRequest()) {
			if ($page != 0)
				show_404();

			$result = $this->_getListTask($slug, array('page' => $page, 'show' => 'opened'));

			//count opened and closed tasks
			$result['count_opened'] = $this->Tasks_model->count_where(array('status !=' => 3, 'idProject' => $result['id']));
			$result['count_closed'] = $this->Tasks_model->count_where(array('status' => 3, 'idProject' => $result['id']));

			$header = array('title' => $result['name']);
			// Render page
			$this->render(array('pages/projects/show' => $result), $header + $this->leftMenu);
		} else {
			$options = array(
				'page' => $page, 
				'show' => 'closed',
			);
			$options['page'] = $page;
			if (isset($_POST['show']))
				$options['show'] = $_POST['show'];

			$result = $this->_getListTask($slug, $options);
			$output['list'] = $result['widget']['tasklist'];
			// Render particial
			$this->load->view('default/ajaxTaskList', $output);
		}
	}

	function _getListTask($slug, $options) {
		// Load library for widget
		$this->load->library('table');
		$this->load->library('showhelper');
		$this->load->library('pagination');

		// Get data
		if(!$result = $this->Projects_model->get_by_slug($slug))
			show_404();

		$result['slug'] = $slug;

		//Send to the widget
		$tasklistData = array(
			'page' => $options['page'],
			'view' => $this->templatePath.'/widgets/tasklist',
			'data' => $result,
			'show' => $options['show'],
		);
		$result['widget']['tasklist'] = $this->widget->get('tasklist', $tasklistData);
		return $result;
	}

}

