<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CMS_Controller {

	public $leftMenu;

	function __construct() {
		parent::__construct('manager');
		$arrayMenu['links'] = array(
			'/manager' => 'Статистика',
			'/manager/welcome/config' => 'Настройки',
		);
		$this->leftMenu['widget']['leftmenu'] = $this->widget->get('menu', array('data' => $arrayMenu, 'view' => $this->templatePath.'/widgets/leftmenu'));
	}

	public function index() {
		$header = array('title' => 'Welcome');
		$result = array('h1' => 'Welcome');
		$this->render(array('pages/default/index' => $result), $header + $this->leftMenu);
	}

	public function config() {
		$header = array('title' => 'Configuration');
		$result = array('h1' => 'Configuration');
		$this->render(array('pages/default/index' => $result), $header + $this->leftMenu);
	}

}

