<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Text extends CMS_Controller {

	function __construct() {
		parent::__construct();
	}

	public function index() {
		show_404();
	}

	public function show($id = false) {
		if ($id == false && !is_int($id)) {
			show_404();
			return;
		}
		$this->load->model('Text_model', '', TRUE);
		$result = $this->Text_model->get_where($id);
		$this->render(array('text/text' => $result), $result);
	}

}

