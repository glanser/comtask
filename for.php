<?php
    function googleParser($q, $site, $region, $start = 0, $gdsess = null) {
        echo 'Начинаю поиск запроса: "'.$q.'" начиная с '.$start.' позиции'.PHP_EOL;
        if(!empty($gdsess)) $region .= '; GDSESS='.$gdsess;
        
        $opts = array(
            'http'=>array(
                'method' => "GET",
                'header' => "Accept-language: ru\r\n" .
                    "Cookie: PREF=".$region.";\r\n" .
                    "user_agent: Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36"
           ),
        );
        $context = stream_context_create($opts);
        $query = urlencode($q);
        if ($start != 0) $query .= '&start='.$start;
        if ($file = @file_get_contents('http://www.google.ru/search?q='.$query, false, $context)) {
            $resultQuery = $file;
        } else {
            echo "Вылезла капча мать ее ети! Срочно куку на тест!".PHP_EOL;
            return 'captcha';
        }

        $resultQuery = preg_replace('#.*<div id="ires"><ol>(.*)</ol></div>#is', '<ol>$1</ol>', $resultQuery);
        preg_match_all('#<li class="g"( [^i].*)?>(.*)</li>#isU', $resultQuery, $resultMatches);

        foreach($resultMatches[2] as $key => $value) {
            preg_match('#<h3 class="r"(.*)?><a href="(.*)"#isU', $value, $partUrl);
            if (substr($partUrl[2], 0, 2) == '/i' || substr($partUrl[2], 0, 21) == 'http://maps.google.ru') continue;
            $resultFull[$key]['position'] = $key + 1;
            $resultFull[$key]['url'] = urldecode(preg_replace('#/url\?q=(.*)&amp;sa=.*#is', '$1', $partUrl[2]));
            if (preg_match('#(http://'.$site.'|http://[a-z\-]+\.'.$site.')#is', $resultFull[$key]['url'])) {
                $resultOne[] = array(
                    'position' => $resultFull[$key]['position'] + $start,
                    'url' => $resultFull[$key]['url'],
                );
            }
        }
        if (!empty($resultOne)) {
            echo "На позиции ".$resultOne[0]['position']." найдена ссылка ".$resultOne[0]['url'].PHP_EOL;
        } else {    
            echo "Нет найденных позиций".PHP_EOL;
        }
        $delay = 10 + rand(1,10);
        echo "Иду спать на ".$delay." секунд".PHP_EOL;
        sleep($delay);
        if (!isset($resultOne)) return NULL;
        return $resultOne;
    }


    if (!isset($_SERVER['argv'][1]) || !isset($_SERVER['argv'][2]) || !isset($_SERVER['argv'][3])) {
        echo "Для запуска сборщика необходимо передать три параметра:".PHP_EOL;
        echo "\t Пример: php ".$_SERVER['argv'][0]." 1 1 10".PHP_EOL;
        echo "\t - первый параметр: id ядра".PHP_EOL;
        echo "\t - второй параметр: id региона".PHP_EOL;
        echo "\t - третий параметр: start для google".PHP_EOL;
        die();
    }

    $server_db = "localhost";
    $user_db = "comtask";
    $password_db = "qPdxw3JwMwTunPNV";
    $name_db = "comtask";

    $link  =  mysql_connect($server_db,  $user_db,  $password_db);  
    if(!$link)  die("Не могу ни хера соединиться  MySQL");
    mysql_select_db($name_db)  or  die("Не нашел ни хера такой таблицы:  $name_db:  ".mysql_error()); 
    mysql_query('SET NAMES utf8;');

    $query = "SELECT * FROM `core` WHERE `id` = ".$_SERVER['argv'][1];
    $resQuery = mysql_query($query);
    if (mysql_num_rows($resQuery) == 0) {
        echo 'Ядра с id='.$_SERVER['argv'][1].' не найдено!'.PHP_EOL;
        die();
    }
    $CORE = mysql_fetch_array($resQuery);

    $query = "SELECT * FROM `region` WHERE `id` = ".$_SERVER['argv'][2];
    $resQuery = mysql_query($query);
    if (mysql_num_rows($resQuery) == 0) {
        echo 'Региона с id='.$_SERVER['argv'][2].' не найдено!'.PHP_EOL;
        die();
    }
    $REGION = mysql_fetch_array($resQuery);

    $query = "SELECT * FROM `query` WHERE `idCore` = ".$_SERVER['argv'][1];
    $resQuery = mysql_query($query);
    if (mysql_num_rows($resQuery) == 0) {
        echo 'Ядра с id='.$_SERVER['argv'][1].' не найдено!'.PHP_EOL;
        die();
    }

    $dateStart = time();

    $sqlStart = "INSERT INTO `start` VALUES('', '".$dateStart."', '0', '".$_SERVER['argv'][1]."', '".$_SERVER['argv'][2]."')";
    mysql_query($sqlStart);
    $startId = mysql_insert_id();

    echo "Начинаю собирать позиции по региону ".$REGION['region'].PHP_EOL;;
    sleep(3);

    while($RESULT = mysql_fetch_array($resQuery)) {
        $q = $RESULT['query'];
        $page = 1;
        if (!isset($gdsess)) $gdsess = NULL;

        for ($i = 0; $i <= $_SERVER['argv'][3]; $i = $i+10) {
            for ($z = 0; $z <= 1; $z++) {
                if (isset($resultOne) && $resultOne == 'captcha') {
                    $gdsess = 'ID=cd31212f6390a127:TM=1375133616:C=c:IP=37.235.171.154-:S=APGng0tZa3D8zOGz30Ixu626DtJdPjQz4w';
                }
                $resultOne = googleParser($q, $CORE['site'], $REGION['cookie'], $i, $gdsess);
                if ($resultOne == 'captcha') {
                    $delayCaptcha = 6 + rand(1, 10);
                    echo "Придется попробовать через ".$delayCaptcha." секунд".PHP_EOL;
                    sleep($delayCaptcha);
                    $z--; 
                } else {
                    break;
                }
            }
            $page ++;
            if (!empty($resultOne)) break;
        }

        if (!empty($resultOne)) {
            $insertResult = "INSERT INTO `result` VALUES ('', '".$RESULT['id']."', '".$startId."', '".$page."', '".$resultOne[0]['position']."', '".$resultOne[0]['url']."');";
        } else {
            $insertResult = "INSERT INTO `result` VALUES ('', '".$RESULT['id']."', '".$startId."', '".$page."', '0', '-');";
        }

        mysql_query($insertResult);


        unset($resultOne);
    }

    echo "Сборщик закончил свою работу. Выход".PHP_EOL;

    exec("screen -x google");
